import io
import json
import picamera
import random
import time
import os
from datetime import datetime
from gpiozero import Button, Buzzer


image_path = "../DashCam_Data/images/"


start_record_time = 0.0
previous_buffer = 60
video_length = 60


def file_name_to_time(name):
    return datetime.strptime(name, 'image%Y-%m-%d %H:%M:%S.%f.png').timestamp()


def time_to_file_name(file_name="image", tms=time.time(), file_type=".png"):
    now = time.gmtime(tms)
    return f"{file_name}{now.tm_year}-{now.tm_mon}-{now.tm_mday} {now.tm_hour}:{now.tm_min}:{now.tm_sec}{file_type}"


def test_time_stays_same_after_conversions():
    test_time = int(time.time())
    modified_time = file_name_to_time(time_to_file_name(test_time))
    assert test_time == modified_time,  f"{test_time} != {modified_time}\nDifference: {test_time - modified_time}"


def set_record():
    global start_record_time
    global do_record
    start_record_time = time.time() - previous_buffer
    print(f"Record_time: {start_record_time}")
    do_record = True
    print("Set flag to record video")


def get_record():
    global do_record
    current_record_status = do_record
    do_record = False
    return current_record_status


camera = picamera.PiCamera()
camera.rotation = 90
stream = picamera.PiCameraCircularIO(camera, seconds=previous_buffer)
camera.start_recording(stream, format='h264')
button = Button(2, bounce_time=video_length/2)
button.when_pressed = set_record
buzzer = Buzzer(14)


try:
    while True:
        camera.wait_recording(1)
        if get_record:
            # Keep recording for 10 seconds and only then write the
            # stream to disk
            camera.wait_recording(video_length)
            stream.copy_to(image_path + time_to_file_name(file_name="video", tms=time.time(), file_type=".h264"))
            print("Saved recording")
            try:
                assert  time.time() - start_record_time == video_length + previous_buffer, f"Error in recording duration: {time.time() - start_record_time} != {video_length + previous_buffer}"
            except AssertionError as e:
                print(e)
except KeyboardInterrupt:
    print("Exiting on KeyboardInterrupt")
finally:
    camera.stop_recording()
    for i in range(2):
        time.sleep(.125)
        buzzer.on()
        time.sleep(.125)
        buzzer.off()
